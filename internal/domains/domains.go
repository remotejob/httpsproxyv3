package domains

import "time"

type Tlsdomain struct {
	Created  time.Time `json:"created"`
	Updated  time.Time `json:"updated"`
	Domain        string `json:"domain"`
	CertURL       string `json:"certUrl"`
	CertStableURL string `json:"certStableUrl"`
	Certificate []byte 
	PrivateKey []byte
}
