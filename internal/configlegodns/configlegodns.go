package configlegodns

import (
	"database/sql"
	"log"

	"github.com/spf13/viper"

	_ "github.com/mattn/go-sqlite3"
)

type Constants struct {
	DigiatlOceanAuthToken string
	Dns                   string

	Sqlitedb struct {
		Dblocation string
	}
}

type Config struct {
	Constants
	Db *sql.DB
}

func New() (*Config, error) {
	config := Config{}
	constants, err := initViper()
	if err != nil {
		return &config, err
	}
	config.Constants = constants

	litedb, err := sql.Open("sqlite3", config.Sqlitedb.Dblocation)
	if err != nil {
		return &config, err

	}

	config.Db = litedb

	return &config, err

}

func initViper() (Constants, error) {
	viper.SetConfigName("legodns.config") // Configuration fileName without the .TOML or .YAML extension
	viper.AddConfigPath(".")              // Search the root directory for the configuration file
	err := viper.ReadInConfig()           // Find and read the config file
	if err != nil {                       // Handle errors reading the config file
		return Constants{}, err
	}

	if err = viper.ReadInConfig(); err != nil {
		log.Panicf("Error reading config file, %s", err)
	}

	var constants Constants
	err = viper.Unmarshal(&constants)
	return constants, err
}
