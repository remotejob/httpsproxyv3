package confighttpsproxyv3

import (
	"database/sql"
	"log"

	"github.com/spf13/viper"

	_ "github.com/mattn/go-sqlite3"
)

type Constants struct {

	Sqlitedb struct {
		Dblocation string
	}

	// Tlsdomains struct {

	// 	Tlsdomain
	// }
	// PORT         string
	// Imageservice struct {
	// 	Url string
	// }
	// Mlservice struct {
	// 	Url string
	// }
	// Wsserver struct {
	// 	Url string
	// }
	// Sqlitedb struct {
	// 	Dblocation string
	// }
	// Greetings struct {
	// 	Url string
	// }
	// Helloagain struct {
	// 	Url string
	// }

}

type Config struct {
	Constants
	Db              *sql.DB

}

func New() (*Config, error) {
	config := Config{}
	constants, err := initViper()
	if err != nil {
		return &config, err
	}
	config.Constants = constants

	// wsclients := make(map[string]domains.Wsclient)
	// config.Wsclients = wsclients
	// broadcast := make(chan domains.Broadcastmsg) // broadcast channel
	// config.Broadcast = broadcast

	// imgs, err := images.Get(config.Imageservice.Url, 9, true)
	// if err != nil {
	// 	return &config, err
	// }
	// config.Imgs = imgs

	// config.Defaultimg = imgs[rand.Intn(len(imgs))]

	litedb, err := sql.Open("sqlite3", config.Sqlitedb.Dblocation)
	if err != nil {
		return &config, err

	}

	config.Db = litedb

	// csvfile, err := os.Open(config.Constants.Greetings.Url)
	// if err != nil {
	// 	log.Fatalln("Couldn't open the csv file", err)
	// }
	// r := csv.NewReader(bufio.NewReader(csvfile))
	// r.Comma = '\t'
	// result, err := r.ReadAll()
	// if err != nil {
	// 	log.Fatalln("Couldn't open the csv file", err)
	// }
	// for _, res := range result {

	// 	config.Greetingslice = append(config.Greetingslice, res[0])

	// }

	// csvfile2, err := os.Open(config.Constants.Helloagain.Url)
	// if err != nil {
	// 	log.Fatalln("Couldn't open the csv file", err)
	// }
	// r2 := csv.NewReader(bufio.NewReader(csvfile2))
	// r2.Comma = '\t'
	// result2, err := r2.ReadAll()
	// if err != nil {
	// 	log.Fatalln("Couldn't open the csv file", err)
	// }

	// for _, res2 := range result2 {

	// 	config.Helloagainslice = append(config.Helloagainslice, res2[0])

	// }

	return &config, err

}

func initViper() (Constants, error) {
	viper.SetConfigName("httpsproxyv3.config") // Configuration fileName without the .TOML or .YAML extension
	viper.AddConfigPath(".")               // Search the root directory for the configuration file
	err := viper.ReadInConfig()            // Find and read the config file
	if err != nil {                        // Handle errors reading the config file
		return Constants{}, err
	}

	// viper.SetDefault("PORT", "8000")
	if err = viper.ReadInConfig(); err != nil {
		log.Panicf("Error reading config file, %s", err)
	}

	var constants Constants
	err = viper.Unmarshal(&constants)
	return constants, err
}
