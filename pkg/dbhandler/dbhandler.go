package dbhandler

import (
	"database/sql"

	"log"

	"gitlab.com/remotejob/httpsproxyv3/internal/domains"
)

func Insert(db *sql.DB, stmtstr string, args ...interface{}) error {

	stmt, err := db.Prepare(stmtstr)
	if err != nil {
		return err
	}

	_, err = stmt.Exec(args[0].(string), args[1].(string), args[2].(string), args[3].([]byte), args[4].([]byte))
	if err != nil {
		return err
	}
	return nil
}

func GetAllCrts(db *sql.DB, stmtstr string) ([]domains.Tlsdomain, error) {

	var res []domains.Tlsdomain

	rows, err := db.Query(stmtstr)
	if err != nil {
		return nil, err
	}
	for rows.Next() {

		var tlsdomain domains.Tlsdomain

		if err := rows.Scan(&tlsdomain.Created, &tlsdomain.Updated, &tlsdomain.Domain, &tlsdomain.CertURL, &tlsdomain.CertStableURL, &tlsdomain.Certificate, &tlsdomain.PrivateKey); err != nil {
			log.Fatal(err)
		}

		res = append(res, tlsdomain)

	}

	return res, nil

}
