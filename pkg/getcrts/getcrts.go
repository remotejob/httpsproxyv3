package getcrts

import (
	"crypto/tls"

	"gitlab.com/remotejob/httpsproxyv3/internal/confighttpsproxyv3"
	"gitlab.com/remotejob/httpsproxyv3/pkg/dbhandler"
)

func Getcrts(conf *confighttpsproxyv3.Config) ([]tls.Certificate, error) {

	var crts []tls.Certificate
	dbcerts, err := dbhandler.GetAllCrts(conf.Db, "select * from tlsdomains")
	if err != nil {
		return nil, err
	}

	for _, dbcert := range dbcerts {

		cert, err := tls.X509KeyPair(dbcert.Certificate, dbcert.PrivateKey)
		if err != nil {
			return nil, err
		}

		crts = append(crts, cert)

	}

	return crts, nil
}
