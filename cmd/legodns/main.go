package main

import (
	"crypto"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"log"
	"time"

	"github.com/go-acme/lego/v4/certcrypto"
	"github.com/go-acme/lego/v4/certificate"
	"github.com/go-acme/lego/v4/challenge/dns01"
	"github.com/go-acme/lego/v4/lego"
	"github.com/go-acme/lego/v4/providers/dns/digitalocean"
	"github.com/go-acme/lego/v4/registration"
	"gitlab.com/remotejob/httpsproxyv3/internal/configlegodns"
	"gitlab.com/remotejob/httpsproxyv3/pkg/dbhandler"
)

// You'll need a user or account type that implements acme.User
type MyUser struct {
	Email        string
	Registration *registration.Resource
	key          crypto.PrivateKey
}

func (u *MyUser) GetEmail() string {
	return u.Email
}
func (u MyUser) GetRegistration() *registration.Resource {
	return u.Registration
}
func (u *MyUser) GetPrivateKey() crypto.PrivateKey {
	return u.key
}

var (
	conf      *configlegodns.Config
	authToken string
	err       error
)

func init() {
	conf, err = configlegodns.New()
	if err != nil {
		log.Panicln("Configuration error", err)
	}
	authToken = conf.DigiatlOceanAuthToken

}
func main() {

	// domains := []string{"*.infochat.fi", "infochat.fi"}
	// domains := []string{"*.treffit.mobi", "treffit.mobi"}
	domains := []string{"*.sexclub.fi", "sexclub.fi"}
	// domains := []string{"devit.pro"}
	// domains := []string{"*.devit.pro"}

	// Create a user. New accounts need an email and private key to start.
	privateKey, err := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	if err != nil {
		log.Fatal(err)
	}

	myUser := MyUser{
		Email: "aleksander.mazurov@gmail.com",
		key:   privateKey,
	}

	config := lego.NewConfig(&myUser)
	config.Certificate.KeyType = certcrypto.RSA2048

	// A client facilitates communication with the CA server.
	client, err := lego.NewClient(config)
	if err != nil {
		log.Fatal(err)
	}
	prconfig := digitalocean.NewDefaultConfig()
	prconfig.AuthToken = authToken
	prconfig.PropagationTimeout = 7200
	prconfig.PollingInterval = 90

	p, err := digitalocean.NewDNSProviderConfig(prconfig)
	if err != nil {
		panic(err)
	}

	opt0 := dns01.DisableCompletePropagationRequirement()
	opt1 := dns01.AddDNSTimeout(time.Second * 60)
	// opt1 := dns01.

	err = client.Challenge.SetDNS01Provider(p, opt0, opt1)
	if err != nil {
		log.Fatal(err)
	}
	// New users will need to register
	reg, err := client.Registration.Register(registration.RegisterOptions{TermsOfServiceAgreed: true})
	if err != nil {
		log.Fatal(err)
	}
	myUser.Registration = reg

	request := certificate.ObtainRequest{
		Domains: domains,
		Bundle:  true,
	}
	certificates, err := client.Certificate.Obtain(request)
	if err != nil {
		log.Fatal(err)
	}

	// Each certificate comes back with the cert bytes, the bytes of the client's
	// private key, and a certificate URL. SAVE THESE TO DISK.
	// fmt.Printf("%#v\n", certificates)

	for _, v := range domains {

		err = dbhandler.Insert(conf.Db, "replace into tlsdomains(domain,certUrl,certStableUrl,crt,key) values(?,?,?,?,?)", v, certificates.CertURL, certificates.CertStableURL, certificates.Certificate, certificates.PrivateKey)
		if err != nil {
			log.Fatal(err)
		}
	}

	// file, err := os.OpenFile(
	// 	"cert.txt",
	// 	os.O_WRONLY|os.O_TRUNC|os.O_CREATE,
	// 	0666,
	// )
	// if err != nil {
	// 	log.Fatal(err)
	// }
	// defer file.Close()

	// _, err = file.Write(certificates.Certificate)
	// if err != nil {
	// 	log.Fatal(err)
	// }

	// // log.Printf("Wrote %d bytes.\n", bytesWritten)

	// file1, err := os.OpenFile(
	// 	"key.txt",
	// 	os.O_WRONLY|os.O_TRUNC|os.O_CREATE,
	// 	0666,
	// )
	// if err != nil {
	// 	log.Fatal(err)
	// }
	// defer file1.Close()

	// _, err = file1.Write(certificates.PrivateKey)
	// if err != nil {
	// 	log.Fatal(err)
	// }

}
