package main

import (
	"crypto/tls"
	"log"
	"net"
	"net/http"
	"net/http/httputil"
	"net/url"

	"github.com/ip2location/ip2location-go"
	"gitlab.com/remotejob/httpsproxyv3/internal/confighttpsproxyv3"
	"gitlab.com/remotejob/httpsproxyv3/pkg/getcrts"
)


type handler struct {
	proxy *httputil.ReverseProxy
}

func (h handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	ip, _, err := net.SplitHostPort(r.RemoteAddr)
	if err != nil {
		log.Panicln(err)
	}

	netIP := net.ParseIP(ip)

	if netIP != nil {

		location, err := ipdb.Get_country_short(ip)

		if err != nil {
			log.Panicln(err)
		}

		log.Println(location.Country_short)

		r.Header.Set("X-Country", location.Country_short)

	}

	h.proxy.ServeHTTP(w, r)
}


func redirect(w http.ResponseWriter, req *http.Request) {

	log.Println(req.RemoteAddr)
	// remove/add not default ports from req.Host
	target := "https://" + req.Host + req.URL.Path
	if len(req.URL.RawQuery) > 0 {
		target += "?" + req.URL.RawQuery
	}
	log.Printf("redirect to: %s", target)
	http.Redirect(w, req, target,
		// see comments below and consider the codes 308, 302, or 301
		http.StatusTemporaryRedirect)
}

var (
	conf  *confighttpsproxyv3.Config
	err   error
	certs []tls.Certificate
	ipdb  *ip2location.DB
)

func init() {
	conf, err = confighttpsproxyv3.New()

	if err != nil {
		log.Panicln("Configuration error", err)
	}

	certs, err = getcrts.Getcrts(conf)

	if err != nil {
		log.Panicln("Configuration error", err)
	}

	ipdb, err = ip2location.OpenDB("db/IP2LOCATION-LITE-DB1.BIN")

	if err != nil {
		return
	}

}
func main() {

	url, err := url.Parse("http://localhost:8000")
	if err != nil {
		panic(err)
	}
	director := func(req *http.Request) {
		req.URL.Scheme = url.Scheme
		req.URL.Host = url.Host
	}

	reverseProxy := &httputil.ReverseProxy{Director: director}
	handler := handler{proxy: reverseProxy}

	go http.ListenAndServe(":80", http.HandlerFunc(redirect))

	s := &http.Server{Addr: ":443", Handler: handler, TLSConfig: &tls.Config{Certificates: certs}}

	log.Println("Listening on :443,https...")
	log.Fatal(s.ListenAndServeTLS("", ""))

}
