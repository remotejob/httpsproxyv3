sqlite3 db/tlsdomains.db
create table tlsdomains (

  created timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  domain varchar NOT NULL PRIMARY KEY,
  certUrl varchar NOT NULL,
  certStableUrl varchar NOT NULL,
  crt BLOB,
  key BLOB

);


go run gitlab.com/remotejob/httpsproxyv3/cmd/httpsproxyv3
go run gitlab.com/remotejob/httpsproxyv3/cmd/digtaldomains
go run gitlab.com/remotejob/httpsproxyv3/cmd/legodns
go run gitlab.com/remotejob/httpsproxyv3/cmd/checkip

go build -o bin/httpsproxyv3  gitlab.com/remotejob/httpsproxyv3/cmd/httpsproxyv3 && \
scp bin/httpsproxyv3 root@104.131.52.116:gorepos/gitlab.com/remotejob/httpsproxyv3/cmd/httpsproxyv3/

curl -L -A "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)" https://devit.pro
nmap -p 443 --script ssl-cert testssl.123video.xyz
nmap -p 443 --script ssl-cert devit.pro


#!!!change db in legodns.config.toml !!!
go run ./cmd/legodns

sqlite3 db/tlsdomains.db.dev
select domain from tlsdomains;


git tag -a v0.0 -m "my version" && git push origin --tags


ssh -Tvvv git@gitlab.com
